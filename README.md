# rnsort

ReName SORTed files

This tool will get the list of files from user input. Then sort the files in
alphabetical order and assign numeric names to the files.

## Usage

```bash
rnsort [-head INT] [GLOBS]
```

## Arguments

### head

The first number result file should start from. Default to 1

### zfill

Number of digits for the result. Default to -1

If zfill is -1, then the number of digits will be determined automatically. 
Where the minimum number of digit will be 2 for automatic determination.

## File input

If the input is not pointing to a file, it will be treated as a glob pattern.

The pattern is implemented with [doublestar](https://github.com/bmatcuk/doublestar#patterns).

Note that negate pattern (`!`) is not implemented
