all: clean build

test:
	go test ./...

lint:
	golangci-lint run -v

clean:
	rm -rf target

build: windows darwin linux

BINARY := rnsort

windows:
	GOOS=windows GOARCH=amd64 go build -o target/$(BINARY)-windows-amd64.exe

darwin:
	GOOS=darwin GOARCH=amd64 go build -o target/$(BINARY)-darwin-amd64

linux:
	GOOS=linux GOARCH=amd64 go build -o target/$(BINARY)-linux-amd64
