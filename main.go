package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/bmatcuk/doublestar"
)

// argument contains all cli argument user passed into the program.
type argument struct {
	// Start of the index where the rename should start counting
	Head int
	// Number of padding. Default is -1
	Zfill int
	// Slice of files for renaming.
	// The file should exist. But read permission is unknown
	// The slice of files should be sorted
	Files []string
}

type plan struct {
	// Old is the original file name
	Old string
	// New is the new file name
	New string
	// Count is the index for the new file
	Count int
}

func parseArgs() (*argument, error) {
	head := flag.Int("head", 1, "Start of the index where rename should start from")
	zfill := flag.Int("zfill", -1, "Number of digits for leftpad. -1 for auto detect")

	flag.Parse()

	globs := flag.Args()

	expandedFiles := make([]string, 0)

	for _, pattern := range globs {
		if stat, err := os.Stat(pattern); err == nil && !stat.IsDir() {
			// The pattern is in fact pointing to an existing file
			expandedFiles = append(expandedFiles, pattern)
			continue
		}

		files, err := doublestar.Glob(pattern)
		if err != nil {
			return nil, err
		}

		expandedFiles = append(expandedFiles, files...)
	}

	sort.Strings(expandedFiles)

	return &argument{
		Head:  *head,
		Zfill: *zfill,
		Files: expandedFiles,
	}, nil
}

// deduceExtension will find all the extensions from given file.
// If all the extensions are the same, then that extension will be returned.
// Otherwise, error will be returned
// The returned extension will contain a prefix dot (e.g. ".js")
func deduceExtension(files []string) (string, error) {
	if len(files) == 0 {
		return "", errors.New("No files were passed in")
	}

	extension := ""

	for _, file := range files {
		ext := filepath.Ext(file)

		if extension == "" {
			extension = ext
		} else if ext != extension {
			return "", errors.New("inconsistent extensions")
		}
	}

	return extension, nil
}

// intLength deduces the number of digits of an int
func intLength(n int) int {
	if n == 0 {
		return 0
	}

	log := math.Log10(float64(n))

	return int(log) + 1
}

// planRename will produce a plan that maps original files to the new filename
func planRename(args *argument, ext string) []plan {
	res := make([]plan, len(args.Files))

	// Maximum number to be used in this operation
	zfill := args.Zfill

	if zfill == -1 {
		max := len(args.Files) + args.Head - 1
		zfill = intLength(max)

		if zfill < 2 {
			zfill = 2
		}
	}

	for i, file := range args.Files {
		count := args.Head + i
		output := fmt.Sprintf("%0*d%s", zfill, count, ext)
		res[i] = plan{
			Old:   file,
			New:   output,
			Count: count,
		}
	}

	return res
}

// printPlan will print the rename plan to stdout
func printPlan(plans []plan) {
	for _, p := range plans {
		count := strconv.Itoa(p.Count)
		colorCount := fmt.Sprintf("\033[0;1;33m%s\033[0m", count)
		old := strings.ReplaceAll(p.Old, count, colorCount)

		fmt.Printf("%s ➡ %s", old, p.New)
		if !strings.Contains(p.Old, count) {
			fmt.Print("\033[0;1;35m (Does not contain the count)\033[0m")
		}
		fmt.Println()
	}
}

// readConfirm will ask user for confirmation
// For unrecognized text, false will be returned
// For error when reading stdin, the error will be returned
func readConfirm() (bool, error) {
	fmt.Print("Would you like to execute the plan? [Y/n] ")

	scanner := bufio.NewScanner(os.Stdin)
	if !scanner.Scan() {
		// Reached EOF. Treat as user terminating the process
		return false, nil
	}

	text := scanner.Text()
	if err := scanner.Err(); err != nil {
		return false, err
	}

	trimmed := strings.Trim(text, " ")
	if strings.EqualFold(trimmed, "y") {
		return true, nil
	}
	return false, nil
}

func exec(plans []plan) error {
	printPlan(plans)
	confirm, err := readConfirm()
	if err != nil {
		return err
	}
	if !confirm {
		fmt.Println("Operation aborted")
		return nil
	}

	for _, p := range plans {
		err := os.Rename(p.Old, p.New)
		if err != nil {
			return err
		}
	}

	fmt.Println("Rename finished")

	return nil
}

func main() {
	args, err := parseArgs()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to parse argument: %s\n", err)
		os.Exit(1)
	}

	ext, err := deduceExtension(args.Files)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to deduce extension: %s\n", err)
		os.Exit(1)
	}

	plans := planRename(args, ext)

	err = exec(plans)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute rename plan: %s\n", err)
		os.Exit(1)
	}
}
