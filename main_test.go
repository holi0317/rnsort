package main

import "testing"

func TestDeduceExtension(t *testing.T) {
	t.Run("Empty input", func(t *testing.T) {
		input := make([]string, 0)
		_, err := deduceExtension(input)
		if err == nil {
			t.Errorf("Expected error to be something. But it is nil")
		}
	})

	t.Run("Inconsistent extension", func(t *testing.T) {
		input := []string{"foo.js", "bar.js", "baz.tgz"}
		_, err := deduceExtension(input[:])
		if err == nil {
			t.Errorf("Expected error to be something. Got nil")
		}
	})

	t.Run("Success case", func(t *testing.T) {
		input := []string{"folder/foo.js", "bar.js"}
		ext, err := deduceExtension(input[:])
		if err != nil {
			t.Errorf("Expected error to be nil for success case. Got %s", err)
		}

		if ext != ".js" {
			t.Errorf("Expected extension to be .js, got %s", ext)
		}
	})

	t.Run("Empty extension", func(t *testing.T) {
		input := []string{""}
		ext, err := deduceExtension(input[:])
		if err != nil {
			t.Errorf("Expected error to e nil. Got %s", err)
		}

		if ext != "" {
			t.Errorf("Expected extension to be empty. Got %s", ext)
		}
	})
}

func TestIntLength(t *testing.T) {

	table := []struct {
		x int
		y int
	}{
		{0, 0},
		{1, 1},
		{9, 1},
		{10, 2},
		{99, 2},
		{183472141, 9},
	}

	for _, tt := range table {
		t.Run(string(tt.x), func(t *testing.T) {
			output := intLength(tt.x)
			if output != tt.y {
				t.Errorf("Expected %d, got %d", tt.y, output)
			}
		})
	}
}
